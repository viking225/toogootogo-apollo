# APOLLO TOOGOTOGO SERVER

## WHAT

A small apollo server who can serve toogootogo shops, permit users to subscribe to them, notify users when shops they subscribe to have new information

## WHY

At first i wanted to create a node server with some graphql endpoints using [express-graphql](https://github.com/graphql/express-graphql)

After some research i heard about apollo framework, and i wanted to test it with some "real world issue"

## HOW

Shops are served by a route from the toogootogo api who is strangely open ?

Users come from a SQL database, managed through sequelize.

Tests are made with jest ?
