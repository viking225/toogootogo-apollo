const { ApolloServer } = require('apollo-server');
const { app: appConfig } = require('config');
const typeDefs = require('./schemas');
const resolvers = require('./resolvers');
const { createStore } = require('./store');

// API
const ShopsAPI = require('./datasources/shops');
const UsersAPI = require('./datasources/users');

const storeOptions = {
};

const store = createStore(storeOptions)

const dataSources = ()  =>  ({
    shopsAPI: new ShopsAPI(),
    usersAPI: new UsersAPI({store})
})

const context = async() =>  {
    return {};
};

const Server = new ApolloServer({
    context,
    typeDefs,
    resolvers,
    dataSources,
    playground: true
});

if (appConfig.env !== 'test') {
    Server.listen({
        port: appConfig.port
    }).then(({url}) =>  console.log(`App running at ${url}`))
}

// for testing
module.exports = {
    dataSources,
    context,
    typeDefs,
    resolvers,
    ApolloServer,
    ShopsAPI,
    UsersAPI,
    store,
    Server
}